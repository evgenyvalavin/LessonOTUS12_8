﻿using Shared_logic_for_dll_s;
using Shared_logic_for_dll_s.Classes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ADO.net_connection
{
    public sealed class UsersTable : IUsersTable
    {
        public async Task CreateUser(User user)
        {
            using SqlConnection conn = new SqlConnection(MainClass.connectString);
            await conn.OpenAsync();
            using SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = $"use OTUShome12_8 INSERT into Users ([first_name],[second_name],[phone],[face_id],[registration_date],[login],[password]) " +
                $"values ({user.FirstName}, {user.SecondName}, {user.Phone}, {user.FaceId}, {user.RegistrationDate}, {user.Login}, {user.Password})";
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task<List<User>> GetAllUsers()
        {
            List<User> users = new List<User>();
            using (SqlConnection conn = new SqlConnection(MainClass.connectString))
            {
                await conn.OpenAsync();
                if (conn.State == System.Data.ConnectionState.Open)
                {
                    using SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "use OTUShome12_8 SELECT [id],[first_name],[second_name],[phone],[face_id],[registration_date],[login],[password]FROM Users";
                    using SqlDataReader reader = await cmd.ExecuteReaderAsync();
                    while (await reader.ReadAsync())
                    {
                        var user = new User
                        {
                            Id = reader.GetInt32(0),
                            FirstName = reader.GetString(1),
                            SecondName = reader.GetString(2),
                            Phone = reader.GetString(3),
                            FaceId = reader.GetInt32(4),
                            RegistrationDate = reader.GetString(5),
                            Login = reader.GetString(6),
                            Password = reader.GetString(7)
                        };
                        users.Add(user);
                    }
                }
                conn.Close();
            }
            return users;
        }

        public async Task DeleteUser(User user)
        {
            string cmdText = @"use OTUShome12_8 Delete FROM Users where [id] like " + user.Id;
            using SqlConnection conn = new SqlConnection(MainClass.connectString);
            await conn.OpenAsync();
            using SqlCommand cmd = new SqlCommand(cmdText, conn);
            await cmd.ExecuteNonQueryAsync ();
            conn.Close();
        }

        public async Task UpdateUserName(User user)
        {
            string cmdText = @"use OTUShome12_8 UPDATE Users SET [first_name] = "+ user.FirstName +" [second_name] ="+ user.SecondName +" WHERE [id] like " + user.Id + "";
            SqlConnection conn = new SqlConnection(MainClass.connectString);
            await conn.OpenAsync();
            using SqlCommand cmd = new SqlCommand(cmdText, conn);
            await cmd.ExecuteNonQueryAsync();
            conn.Close();
        }
    }
}
