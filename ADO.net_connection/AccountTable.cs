﻿using Shared_logic_for_dll_s;
using Shared_logic_for_dll_s.Classes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO.net_connection
{
    public class AccountTable : IAccountTable
    {
        public async Task<List<Account>> GetAccounts(User user)
        {
            List<Account> accounts = new List<Account>();
            using (SqlConnection conn = new SqlConnection(MainClass.connectString))
            {
                await conn.OpenAsync();
                if (conn.State == System.Data.ConnectionState.Open)
                {
                    using SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "use OTUShome12_8 SELECT [id],[opening_date],[balance],[user_id]FROM Accounts";
                    using SqlDataReader reader = await cmd.ExecuteReaderAsync();
                    while (await reader.ReadAsync())
                    {
                        var account = new Account
                        {
                            Id = reader.GetInt32(0),
                            Opening_date = reader.GetString(1),
                            Balance = reader.GetInt32(2),
                            User_id = reader.GetInt32(3),
                        };
                        accounts.Add(account);
                    }
                }
                conn.Close();
            }
            return accounts;
        }

        public async Task RemoveAccount(Account account)
        {
            string cmdText = @"use OTUShome12_8 Delete FROM Accounts where [id] like " + account.Id;
            using SqlConnection conn = new SqlConnection(MainClass.connectString);
            await conn.OpenAsync();
            using SqlCommand cmd = new SqlCommand(cmdText, conn);
            await cmd.ExecuteNonQueryAsync();
            conn.Close();        
        }

        public async Task ChangeBalance(Account account, int newBalance)
        {
            string cmdText = @"use OTUShome12_8 UPDATE Accounts SET [balance] = " + newBalance + " WHERE [id] like " + account.Id + "";
            SqlConnection conn = new SqlConnection(MainClass.connectString);
            await conn.OpenAsync();
            using SqlCommand cmd = new SqlCommand(cmdText, conn);
            await cmd.ExecuteNonQueryAsync();
            conn.Close();
        }

        public async Task CreateAccount(Account account, User user)
        {
            using SqlConnection conn = new SqlConnection(MainClass.connectString);
            await conn.OpenAsync();
            using SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = $"use OTUShome12_8 INSERT into Accounts ([opening_date],[balance],[user_id]) " +
                $"values ({DateTime.Now.ToShortDateString()}, {account.Balance}, {user.Id})";
            await cmd.ExecuteNonQueryAsync();
        }
    }
}
