﻿using Shared_logic_for_dll_s;
using Shared_logic_for_dll_s.Classes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO.net_connection
{
    public class HistoryTable : IHistoryTable
    {
        public async Task ChangeSum(History history, int newSum)
        {
            string cmdText = @"use OTUShome12_8 UPDATE History SET [sum] = " + newSum + " WHERE [id] like " + history.Id + "";
            SqlConnection conn = new SqlConnection(MainClass.connectString);
            await conn.OpenAsync();
            using SqlCommand cmd = new SqlCommand(cmdText, conn);
            await cmd.ExecuteNonQueryAsync();
            conn.Close();
        }

        public async Task CreateHistory(History history, Account account)
        {
            using SqlConnection conn = new SqlConnection(MainClass.connectString);
            await conn.OpenAsync();
            using SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = $"use OTUShome12_8 INSERT into History ([date],[type],[sum],[account_id]) " +
                $"values ({DateTime.Now.ToShortDateString()}, {history.Type}, {history.Sum}, {account.Id})";
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task<List<History>> GetHistory(Account account)
        {
            List<History> histories = new List<History>();
            using (SqlConnection conn = new SqlConnection(MainClass.connectString))
            {
                await conn.OpenAsync();
                if (conn.State == System.Data.ConnectionState.Open)
                {
                    using SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "use OTUShome12_8 SELECT [id],[date],[type],[sum],[account_id]FROM History";
                    using SqlDataReader reader = await cmd.ExecuteReaderAsync();
                    while (await reader.ReadAsync())
                    {
                        var history = new History
                        {
                            Id = reader.GetInt32(0),
                            Date = reader.GetString(1),
                            Type = reader.GetString(2),
                            Sum = reader.GetInt32(3),
                            Account_id = reader.GetInt32(4)
                        };
                        histories.Add(history);
                    }
                }
                conn.Close();
            }
            return histories;
        }

        public async Task RemoveHistory(History history)
        {
            string cmdText = @"use OTUShome12_8 Delete FROM Accounts where [id] like " + history.Id;
            using SqlConnection conn = new SqlConnection(MainClass.connectString);
            await conn.OpenAsync();
            using SqlCommand cmd = new SqlCommand(cmdText, conn);
            await cmd.ExecuteNonQueryAsync();
            conn.Close();
        }
    }
}
