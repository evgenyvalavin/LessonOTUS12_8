﻿using Shared_logic_for_dll_s;
using Shared_logic_for_dll_s.Classes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO.net_connection
{
    public class InvestingTable : IInvesting
    {
        public async Task ChangeInvesting(Investing investing)
        {
            string cmdText = @"use OTUShome12_8 UPDATE [Investing] SET [Stocks_count] = " + investing.Stocks_count + ", [Stock_portfolio_sum] = " + investing.StockSum + " WHERE [id] like " + investing.Id + "";
            SqlConnection conn = new SqlConnection(MainClass.connectString);
            await conn.OpenAsync();
            using SqlCommand cmd = new SqlCommand(cmdText, conn);
            await cmd.ExecuteNonQueryAsync();
            conn.Close();
        }

        public async Task CreateInvesting(Investing investing)
        {
            using SqlConnection conn = new SqlConnection(MainClass.connectString);
            await conn.OpenAsync();
            using SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = $"use OTUShome12_8 INSERT into Investing ([user_id],[Stocks_count],[Stock_portfolio_sum]) " +
                $"values ({investing.User_id}, {investing.Stocks_count}, {investing.StockSum})";
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task<List<Investing>> GetEntry(User user)
        {
            List<Investing> investings = new List<Investing>();
            using (SqlConnection conn = new SqlConnection(MainClass.connectString))
            {
                await conn.OpenAsync();
                if (conn.State == System.Data.ConnectionState.Open)
                {
                    using SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "use OTUShome12_8 SELECT [id],[Stocks_count],[Stock_portfolio_sum] where user_id like "+ user.Id + " FROM Investing";
                    using SqlDataReader reader = await cmd.ExecuteReaderAsync();
                    while (await reader.ReadAsync())
                    {
                        var investing = new Investing
                        {
                            Id = reader.GetInt32(0),
                            Stocks_count = reader.GetInt32(1),
                            StockSum = reader.GetInt32(2),
                        };
                        investings.Add(investing);
                    }
                }
                conn.Close();
            }
            return investings;
        }

        public async Task RemoveInvesting(Investing investing)
        {
            string cmdText = @"use OTUShome12_8 Delete FROM Investing where [id] like " + investing.Id;
            using SqlConnection conn = new SqlConnection(MainClass.connectString);
            await conn.OpenAsync();
            using SqlCommand cmd = new SqlCommand(cmdText, conn);
            await cmd.ExecuteNonQueryAsync();
            conn.Close();
        }
    }
}
