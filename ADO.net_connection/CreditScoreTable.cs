﻿using Shared_logic_for_dll_s;
using Shared_logic_for_dll_s.Classes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO.net_connection
{
    public class CreditScoreTable : ICreditScoreTable
    {
        public async Task ChangeScore(CreditScore creditScore)
        {
            string cmdText = @"use OTUShome12_8 UPDATE [CreditScore] SET [Score] = " + creditScore.Score + " WHERE [id] like " + creditScore.User_id + "";
            SqlConnection conn = new SqlConnection(MainClass.connectString);
            await conn.OpenAsync();
            using SqlCommand cmd = new SqlCommand(cmdText, conn);
            await cmd.ExecuteNonQueryAsync();
            conn.Close();
        }

        public async Task CreateRecorcd(CreditScore creditScore)
        {
            using SqlConnection conn = new SqlConnection(MainClass.connectString);
            await conn.OpenAsync();
            using SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = $"use OTUShome12_8 INSERT into CreditScore ([User_id],[score])" +
                $"values ({creditScore.User_id}, {creditScore.Score})";
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task<List<CreditScore>> GetScore(User user)
        {
            List<CreditScore> creditScores = new List<CreditScore>();
            using (SqlConnection conn = new SqlConnection(MainClass.connectString))
            {
                await conn.OpenAsync();
                if (conn.State == System.Data.ConnectionState.Open)
                {
                    using SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "use OTUShome12_8 SELECT [score] where user_id like "+ user.Id +" FROM CreditScore";
                    using SqlDataReader reader = await cmd.ExecuteReaderAsync();
                    while (await reader.ReadAsync())
                    {
                        var creditScore = new CreditScore
                        {
                            User_id = reader.GetInt32(0),
                            Score = reader.GetInt32(1),
                        };
                        creditScores.Add(creditScore);
                    }
                }
                conn.Close();
            }
            return creditScores;
        }

        public async Task RemoveEntry(CreditScore investing)
        {
            string cmdText = @"use OTUShome12_8 Delete FROM CreditScore where [user_id] like " + investing.User_id;
            using SqlConnection conn = new SqlConnection(MainClass.connectString);
            await conn.OpenAsync();
            using SqlCommand cmd = new SqlCommand(cmdText, conn);
            await cmd.ExecuteNonQueryAsync();
            conn.Close();
        }
    }
}
