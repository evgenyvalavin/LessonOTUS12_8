﻿using Shared_logic_for_dll_s.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared_logic_for_dll_s
{
    public interface IInvesting
    {
        Task<List<Investing>> GetEntry(User user);
        Task CreateInvesting(Investing investing);
        Task RemoveInvesting(Investing investing);
        Task ChangeInvesting(Investing investing);
    }
}
