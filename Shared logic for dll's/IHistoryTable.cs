﻿using Shared_logic_for_dll_s.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared_logic_for_dll_s
{
    public interface IHistoryTable
    {
        Task<List<History>> GetHistory(Account account);
        Task RemoveHistory(History history);
        Task ChangeSum(History history, int newSum);
        Task CreateHistory(History history, Account account);
    }
}
