﻿using Shared_logic_for_dll_s.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared_logic_for_dll_s
{
    public interface IAccountTable
    {
        Task<List<Account>> GetAccounts(User user);
        Task RemoveAccount(Account account);
        Task ChangeBalance(Account account, int newBalance);
        Task CreateAccount(Account account, User user);
    }
}
