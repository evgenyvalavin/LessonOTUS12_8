﻿using Shared_logic_for_dll_s.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared_logic_for_dll_s
{
    public interface ICreditScoreTable
    {
        Task<List<CreditScore>> GetScore(User user);
        Task CreateRecorcd(CreditScore creditScore);
        Task ChangeScore(CreditScore creditScore);
        Task RemoveEntry(CreditScore creditScore);
    }
}
