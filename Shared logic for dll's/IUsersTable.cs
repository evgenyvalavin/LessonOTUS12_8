﻿using Shared_logic_for_dll_s.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared_logic_for_dll_s
{
    public interface IUsersTable
    {
        Task<List<User>> GetAllUsers();
        Task CreateUser(User user);
        Task DeleteUser(User user);
        Task UpdateUserName(User user);
    }
}
