﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared_logic_for_dll_s.Classes
{
    public enum OperationType { Refill, Withdraw };

    public class History
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string Type { get; set; }
        public int Sum { get; set; }
        public int Account_id { get; set; }
    }
}
