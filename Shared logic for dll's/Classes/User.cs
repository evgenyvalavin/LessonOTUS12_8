﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared_logic_for_dll_s.Classes
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Phone { get; set; }
        public int FaceId { get; set;}
        public string RegistrationDate { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
