﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared_logic_for_dll_s.Classes
{
    public class CreditScore
    {
        public int User_id { get; set; }
        public int Score { get; set; }
    }
}
