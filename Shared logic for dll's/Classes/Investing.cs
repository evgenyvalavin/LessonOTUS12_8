﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared_logic_for_dll_s.Classes
{
    public class Investing
    {
        public int Id { get; set; }
        public int User_id { get; set; }
        public int Stocks_count { get; set; }
        public int StockSum { get; set; }
    }
}
