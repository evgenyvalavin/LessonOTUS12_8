﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared_logic_for_dll_s.Classes
{
    public  class Account
    {
        public int Id { get; set; }
        public string Opening_date { get; set; }
        public int Balance { get; set; }
        public int User_id { get; set; }
    }
}
