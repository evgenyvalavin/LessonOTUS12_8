﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using EF6_connection;
using ADO.net_connection;
//using Shared_logic_for_dll_s;
using User = Shared_logic_for_dll_s.Classes.User;

namespace ConsoleApp
{
    class Program
    {
        static readonly UsersTable UsersTableOperations = new UsersTable();

        static async Task Main(string[] args)
        {
            var users = await UsersTableOperations.GetAllUsers();
            var user = new User
            {
                RegistrationDate = DateTime.Today.ToShortDateString(),
                FaceId = DateTime.Now.Second + DateTimeOffset.Now.Millisecond,
                FirstName = $"Name {DateTime.Now.DayOfYear}{DateTime.Now.Second}",
                SecondName = $"SName {DateTime.Now.DayOfYear}{DateTime.Now.Minute}",
                Id = DateTimeOffset.Now.Millisecond - DateTime.Now.Second,
                Login = $"logn{DateTime.Now.DayOfYear}{DateTime.Now.Millisecond}",
                Phone = $"+7{DateTime.Now.Day}-2-{DateTime.Now.Day}-{DateTime.UtcNow.Second}",
                Password = $"passw{DateTime.Now.DayOfYear}{DateTime.Now.Second}"
            };
            await UsersTableOperations.CreateUser(user);


        }
    }
}
