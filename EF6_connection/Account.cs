namespace EF6_connection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Account
    {
        public int id { get; set; }

        [StringLength(10)]
        public string opening_date { get; set; }

        public int? balance { get; set; }

        public int? user_id { get; set; }
    }
}
