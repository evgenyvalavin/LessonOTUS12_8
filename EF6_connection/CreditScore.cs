﻿namespace EF6_connection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CreditScore")]
    public partial class CreditScore
    {
        public int? User_id { get; set; }

        public int? Score { get; set; }
    }
}
