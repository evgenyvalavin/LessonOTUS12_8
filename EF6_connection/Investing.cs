namespace EF6_connection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Investing")]
    public partial class Investing
    {
        public int id { get; set; }

        public int? user_id { get; set; }

        public int? Stocks_count { get; set; }

        public int? Stock_portfolio_sum { get; set; }
    }
}
