﻿using Shared_logic_for_dll_s;
using Shared_logic_for_dll_s.Classes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF6_connection
{
    public class AccountTable : IAccountTable
    {
        public async Task ChangeBalance(Shared_logic_for_dll_s.Classes.Account account, int newBalance)
        {
            using var db = new UsersDB();
            var result = db.Accounts.SingleOrDefault(x => x.id == account.Id);
            if (result != null)
            {
                result.balance = account.Balance;
                await db.SaveChangesAsync();
            }
        }

        public async Task CreateAccount(Shared_logic_for_dll_s.Classes.Account account, Shared_logic_for_dll_s.Classes.User user)
        {
            using var db = new UsersDB();
            Account dbAccount = new Account()
            {
                balance = account.Balance,
                id = account.Id,
                opening_date = account.Opening_date,
                user_id = account.User_id
            };
            db.Accounts.Add(dbAccount);
            await db.SaveChangesAsync();
        }

        public async Task<List<Shared_logic_for_dll_s.Classes.Account>> GetAccounts(Shared_logic_for_dll_s.Classes.User user)
        {
            List<Shared_logic_for_dll_s.Classes.Account> accounts = new List<Shared_logic_for_dll_s.Classes.Account>();
            await Task.Run(() =>
            {
                using var db = new UsersDB();
                var query = from b in db.Accounts
                            select b;
                foreach (var item in query)
                {
                    Shared_logic_for_dll_s.Classes.Account account = new Shared_logic_for_dll_s.Classes.Account()
                    {
                        Id = item.id
                    };
                    accounts.Add(account);
                }
            });
            return accounts;
        }

        public async Task RemoveAccount(Shared_logic_for_dll_s.Classes.Account account)
        {
            using var db = new UsersDB();
            db.Entry(account).State = EntityState.Deleted;
            await db.SaveChangesAsync();
        }
    }
}
