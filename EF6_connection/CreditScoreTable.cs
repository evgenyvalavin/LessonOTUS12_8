﻿using Shared_logic_for_dll_s;
using Shared_logic_for_dll_s.Classes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF6_connection
{
    public class CreditScoreTable : ICreditScoreTable
    {
        public async Task ChangeScore(Shared_logic_for_dll_s.Classes.CreditScore creditScore)
        {
            using var db = new UsersDB();
            var result = db.CreditScore.SingleOrDefault(x => x.User_id == creditScore.User_id);
            if (result != null)
            {
                result.Score = creditScore.Score;
                await db.SaveChangesAsync();
            }
        }

        public async Task CreateRecorcd(Shared_logic_for_dll_s.Classes.CreditScore creditScore)
        {
            using var db = new UsersDB();
            CreditScore dbCredit = new CreditScore
            {
                User_id = creditScore.User_id,
                Score = creditScore.Score
            };
            db.CreditScore.Add(dbCredit);
            await db.SaveChangesAsync();
        }

        public async Task<List<Shared_logic_for_dll_s.Classes.CreditScore>> GetScore(Shared_logic_for_dll_s.Classes.User user)
        {
            List<Shared_logic_for_dll_s.Classes.CreditScore> creditScores = new List<Shared_logic_for_dll_s.Classes.CreditScore>();
            await Task.Run(() =>
            {
                using var db = new UsersDB();
                var query = from b in db.CreditScore
                            select b;
                foreach (var item in query)
                {
                    Shared_logic_for_dll_s.Classes.CreditScore score = new Shared_logic_for_dll_s.Classes.CreditScore()
                    {
                        User_id = item.User_id.Value,
                        Score = item.Score.Value
                    };
                    creditScores.Add(score);
                }
            });
            return creditScores;
        }

        public async Task RemoveEntry(Shared_logic_for_dll_s.Classes.CreditScore creditScore)
        {
            using var db = new UsersDB();
            db.Entry(creditScore).State = EntityState.Deleted;
            await db.SaveChangesAsync();
        }
    }
}
