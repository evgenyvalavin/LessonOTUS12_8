﻿using Shared_logic_for_dll_s;
using Shared_logic_for_dll_s.Classes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF6_connection
{
    public class InvestingTable : IInvesting
    {
        public async Task ChangeInvesting(Shared_logic_for_dll_s.Classes.Investing investing)
        {
            using var db = new UsersDB();
            var result = db.Investings.SingleOrDefault(x => x.id == investing.Id);
            if (result != null)
            {
                result.Stocks_count = investing.Stocks_count;
                result.Stock_portfolio_sum = investing.StockSum;
                await db.SaveChangesAsync();
            }
        }

        public async Task CreateInvesting(Shared_logic_for_dll_s.Classes.Investing investing)
        {
            using var db = new UsersDB();
            Investing dbInvest = new Investing
            {
                id = investing.Id,
                Stock_portfolio_sum = investing.StockSum,
                Stocks_count = investing.Stocks_count,
                user_id = investing.User_id
            };
            db.Investings.Add(dbInvest);
            await db.SaveChangesAsync();
        }

        public async Task<List<Shared_logic_for_dll_s.Classes.Investing>> GetEntry(Shared_logic_for_dll_s.Classes.User user)
        {
            List<Shared_logic_for_dll_s.Classes.Investing> investings = new List<Shared_logic_for_dll_s.Classes.Investing>();
            await Task.Run(() =>
            {
                using var db = new UsersDB();
                var query = from b in db.Investings
                            select b;
                foreach (var item in query)
                {
                    Shared_logic_for_dll_s.Classes.Investing investing = new Shared_logic_for_dll_s.Classes.Investing()
                    { 
                        Id = item.id,
                        User_id = item.user_id.Value,
                        Stocks_count = item.Stocks_count.Value,
                        StockSum = item.Stock_portfolio_sum.Value
                    };
                    investings.Add(investing);
                }
            });
            return investings;
        }

        public async Task RemoveInvesting(Shared_logic_for_dll_s.Classes.Investing investing)
        {
            using var db = new UsersDB();
            db.Entry(investing).State = EntityState.Deleted;
            await db.SaveChangesAsync();
        }
    }
}
