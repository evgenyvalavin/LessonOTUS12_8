﻿using Shared_logic_for_dll_s;
using Shared_logic_for_dll_s.Classes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF6_connection
{
    public class UsersTable : IUsersTable
    {
        public async Task CreateUser(Shared_logic_for_dll_s.Classes.User user)
        {
            using var db = new UsersDB();
            User dbUser = new User
            {
                id = user.Id,
                face_id = user.FaceId,
                first_name = user.FirstName,
                login = user.Login,
                password = user.Password,
                phone = user.Phone,
                registration_date = user.RegistrationDate,
                second_name = user.SecondName
            };
            db.Users.Add(dbUser);
            await db.SaveChangesAsync();
        }

        public async Task DeleteUser(Shared_logic_for_dll_s.Classes.User user)
        {
            using var db = new UsersDB();
            db.Entry(user).State = EntityState.Deleted;
            await db.SaveChangesAsync();
        }

        public async Task<List<Shared_logic_for_dll_s.Classes.User>> GetAllUsers()
        {
            List<Shared_logic_for_dll_s.Classes.User> users = new List<Shared_logic_for_dll_s.Classes.User>();
            await Task.Run(() =>
            {
                using var db = new UsersDB();
                var query = from b in db.Users
                            select b;
                foreach (var item in query)
                {
                    Shared_logic_for_dll_s.Classes.User user = new Shared_logic_for_dll_s.Classes.User()
                    {
                        Id = item.id,
                        FaceId = item.face_id.Value,
                        FirstName = item.face_id.Value.ToString(),
                        Login = item.login,
                        Password = item.password,
                        SecondName = item.second_name,
                        Phone = item.phone,
                        RegistrationDate = item.registration_date
                    };
                    users.Add(user);
                }
            });
            return users;
        }

        public async Task UpdateUserName(Shared_logic_for_dll_s.Classes.User user)
        {
            using var db = new UsersDB();
            var result = db.Users.SingleOrDefault(x => x.id == user.Id);
            if (result != null)
            {
                result.first_name = user.FirstName;
                result.second_name = user.SecondName;
                await db.SaveChangesAsync();
            }
        }
    }
}
