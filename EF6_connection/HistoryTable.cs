﻿using Shared_logic_for_dll_s;
using Shared_logic_for_dll_s.Classes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF6_connection
{
    public class HistoryTable : IHistoryTable
    {
        public async Task ChangeSum(Shared_logic_for_dll_s.Classes.History history, int newSum)
        {
            using var db = new UsersDB();
            var result = db.Histories.SingleOrDefault(x => x.id == history.Id);
            if (result != null)
            { 
                result.sum = newSum;
                await db.SaveChangesAsync();
            }
        }

        public async Task CreateHistory(Shared_logic_for_dll_s.Classes.History history, Shared_logic_for_dll_s.Classes.Account account)
        {
            using var db = new UsersDB();
            History dbHist = new History
            {
                account_id = account.Id,
                id = history.Id,
                sum = history.Sum,
                date = history.Date,
                type = history.Type
            };
            db.Histories.Add(dbHist);
            await db.SaveChangesAsync();
        }

        public async Task<List<Shared_logic_for_dll_s.Classes.History>> GetHistory(Shared_logic_for_dll_s.Classes.Account account)
        {
            List<Shared_logic_for_dll_s.Classes.History> histories = new List<Shared_logic_for_dll_s.Classes.History>();
            await Task.Run(() =>
            {
                using var db = new UsersDB();
                var query = from b in db.Histories
                            select b;
                foreach (var item in query)
                {
                    Shared_logic_for_dll_s.Classes.History history = new Shared_logic_for_dll_s.Classes.History()
                    {
                        Id = item.id,
                        Account_id = item.account_id.Value,
                        Sum = item.sum.Value,
                        Date = item.date,
                        Type = item.type
                    };
                    histories.Add(history);
                }
            });
            return histories;
        }

        public async Task RemoveHistory(Shared_logic_for_dll_s.Classes.History history)
        {
            using var db = new UsersDB();
            db.Entry(history).State = EntityState.Deleted;
            await db.SaveChangesAsync();
        }
    }
}
