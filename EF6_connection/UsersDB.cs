namespace EF6_connection
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class UsersDB : DbContext
    {
        public UsersDB()
            : base("name=UsersDB1")
        {
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<History> Histories { get; set; }
        public virtual DbSet<Investing> Investings { get; set; }
        public virtual DbSet<CreditScore> CreditScore { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .Property(e => e.first_name)
                .IsFixedLength();

            modelBuilder.Entity<User>()
                .Property(e => e.second_name)
                .IsFixedLength();

            modelBuilder.Entity<User>()
                .Property(e => e.phone)
                .IsFixedLength();

            modelBuilder.Entity<User>()
                .Property(e => e.registration_date)
                .IsFixedLength();

            modelBuilder.Entity<User>()
                .Property(e => e.login)
                .IsFixedLength();

            modelBuilder.Entity<User>()
                .Property(e => e.password)
                .IsFixedLength();

            modelBuilder.Entity<Account>()
                .Property(e => e.opening_date)
                .IsFixedLength();

            modelBuilder.Entity<History>()
                .Property(e => e.date)
                .IsFixedLength();

            modelBuilder.Entity<History>()
                .Property(e => e.type)
                .IsFixedLength();
        }
    }
}
