namespace EF6_connection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("History")]
    public partial class History
    {
        public int id { get; set; }

        [StringLength(10)]
        public string date { get; set; }

        [StringLength(10)]
        public string type { get; set; }

        public int? sum { get; set; }

        public int? account_id { get; set; }
    }
}
