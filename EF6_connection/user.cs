namespace EF6_connection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User
    {
        public int id { get; set; }

        [StringLength(10)]
        public string first_name { get; set; }

        [StringLength(10)]
        public string second_name { get; set; }

        [StringLength(12)]
        public string phone { get; set; }

        public int? face_id { get; set; }

        [StringLength(14)]
        public string registration_date { get; set; }

        [StringLength(10)]
        public string login { get; set; }

        [StringLength(10)]
        public string password { get; set; }
    }
}
